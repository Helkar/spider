import tkinter as tk
from tkinter import ttk
import sqlite3

class Main(tk.Frame):
    def __init__(self, root):
        super().__init__(root)
        self.init_main()

    def init_main(self):
        toolbar = tk.Frame(bg='#d7d8e0', bd=2, width=1000, height=50)
        toolbar.pack(side=tk.TOP, fill=tk.X, expand=True)

        label_description = tk.Label(toolbar, text='Вид спорта:')
        label_description.place(x=180, y=15)

        btn_ok = ttk.Button(toolbar, text='Искать')
        btn_ok.place(x=420, y=15)
        btn_ok.bind('<Button-1>')

        self.entry_sport = ttk.Entry(toolbar)
        self.entry_sport.place(x=270, y=15)

        
        self.tree = ttk.Treeview(self, columns=('ID', 'title', 'link'),
                                 height=15, show='headings')
        self.tree.column("ID", width=30, anchor=tk.CENTER)
        self.tree.column("title", width=365, anchor=tk.CENTER)
        self.tree.column("link", width=150, anchor=tk.CENTER)

        self.tree.heading("ID", text='ID')
        self.tree.heading("title", text='Заголовок')
        self.tree.heading("link", text='Ссылка')
   

        self.tree.pack()


class DB:
    def __init__(self):
        self.conn = sqlite3.connect('grabed.db')
        self.c = self.conn.cursor()
        self.c.execute(
            '''CREATE TABLE IF NOT EXISTS grabed (id integer primary key, title text, link text)''')
        self.conn.commit()


if __name__ == "__main__":
    root = tk.Tk()
    app = Main(root)
    app.pack()
    root.title("Агрегатор")
    root.geometry("650x450+300+200")
    root.resizable(False, False)
    root.mainloop()